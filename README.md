# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
Archetype use to create a service assembly projet with a sample SOAP project 


* Version
1.0.0-SNAPSHOT : 
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###



* Configuration

Require JDK 7

* How to run tests


* Assembly generation

Just run the command below : 

    mvn archetype:generate \                                                                                    
      -DarchetypeGroupId=net.open-esb.maven.archetypes \
      -DarchetypeArtifactId=service-assembly-archetype \
      -DarchetypeVersion=1.0.0-SNAPSHOT \
      -DgroupId=net.openesb.archetype \
      -Dpackage=net.openesb.archetype \
      -DartifactId=demo \
      -DprocessName=helloWorld \
      -DclusterName=server \
      -Dversion=1.0.0-SNAPSHOT \
      -DtargetNamespace=http://com.mycompany