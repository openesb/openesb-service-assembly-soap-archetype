
# L'utilisateur utilis� pour lancer les commande asadmin.
user=$1

# Le fichier contenant le mot de passe de l'utilisateur.
passwordFile=$2

# La cible concern�e.
target=$3

# Le component concern�.
component=$4

# Chemin vers le zip
chemin=$5

commande=$6


#Test si composant deploy�
var=$($commande list-jbi-service-assemblies --user=$user --passwordfile=$passwordFile   --target=$target)
if test "$?" = "0"
then
	echo "$var"| grep -q "$component" 
	if test "$?" = "0" 
	then
		#Test si composant d�marr�
		var=$($commande show-jbi-service-assembly  --user=$user --passwordfile=$passwordFile   --target=$target $component)
		if test "$?" = "0"
		then
			echo "$var"| grep -q "State         : Started" 
			if test "$?" = "0" 
			then
				$commande stop-jbi-service-assembly --user=$user --passwordfile=$passwordFile --target=$target $component
				if test "$?" = "0"
				then
				  echo "Composant arrete"
					$commande shut-down-jbi-service-assembly --user=$user --passwordfile=$passwordFile --target=$target $component
					if test  "$?" = "0"
					then
						echo "Composant shutDown"
					else
						echo "Probleme lors de l'extinction du composant - Arret du process de livraison"
						exit
					fi
				else
					echo "Probleme lors de l'arret du composant - Arret du process de livraison"
					exit   
				fi	
			fi
		fi
		
		$commande undeploy-jbi-service-assembly --user=$user --passwordfile=$passwordFile --target=$target $component
		if test  "$?" = "0"
		then
			echo "Composant undeploy�"
		else
			echo "Probleme lors de l'undeploy du composant - Arret du process de livraison"
			exit
		fi
	fi

	$commande deploy-jbi-service-assembly --user=$user --passwordfile=$passwordFile --target=$target $chemin
    if  test  "$?" = "0"
    then
      echo "Composant deploy�"
		  $commande start-jbi-service-assembly --user=$user --passwordfile=$passwordFile --target=$target $component
	    if  test  "$?" = "0"
        then
      echo "Composant demarre"  
			echo "Livraison complete"
        else
			echo "Probleme lors du demarrage du nouveau composant - Arret du process de livraison"
        fi
    else
        echo "Probleme lors du deploiement du nouveau composant - Arret du process de livraison"
    fi
else
	echo "Impossible de lister les composants -  Arret du process de livraison"
	exit
fi